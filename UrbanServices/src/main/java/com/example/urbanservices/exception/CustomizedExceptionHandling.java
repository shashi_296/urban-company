package com.example.urbanservices.exception;

import com.example.urbanservices.dto.ExceptionResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomizedExceptionHandling extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UrbanGlobalException.class)
    public ResponseEntity<Object> handleExceptions(UrbanGlobalException exception, WebRequest webRequest) {
        ExceptionResponse response = exception.getExceptionResponse();
        return new ResponseEntity<>(response, exception.getExceptionResponse().getHttpStatus());

    }
}
