package com.example.urbanservices.exception;

import com.example.urbanservices.dto.ExceptionResponse;
import com.example.urbanservices.enums.UrbanException;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Setter
@Getter
public class UrbanGlobalException extends RuntimeException {

    private final ExceptionResponse exceptionResponse;

    public UrbanGlobalException(UrbanException exception) {
        this.exceptionResponse = new ExceptionResponse();
        this.exceptionResponse.setException(exception.name());
        this.exceptionResponse.setMessage(exception.getMessage());
        this.exceptionResponse.setErrorCode(exception.getCode().toString());
        this.exceptionResponse.setHttpStatus(exception.getHttpStatus());
        this.exceptionResponse.setDateTime(LocalDateTime.now());
    }

    public UrbanGlobalException(UrbanException exception, String msg) {
        this.exceptionResponse = new ExceptionResponse();
        this.exceptionResponse.setException(exception.name());
        this.exceptionResponse.setMessage(msg);
        this.exceptionResponse.setErrorCode(exception.getCode().toString());
        this.exceptionResponse.setHttpStatus(exception.getHttpStatus());
        this.exceptionResponse.setDateTime(LocalDateTime.now());
    }

    public UrbanGlobalException(UrbanException exception, String msg, int code) {
        this.exceptionResponse = new ExceptionResponse();
        this.exceptionResponse.setException(exception.name());
        this.exceptionResponse.setMessage(msg);
        this.exceptionResponse.setErrorCode(String.valueOf(code));
        this.exceptionResponse.setHttpStatus(exception.getHttpStatus());
        this.exceptionResponse.setDateTime(LocalDateTime.now());
    }

    public UrbanGlobalException(UrbanException exception, String msg, int code, HttpStatus httpStatus) {
        this.exceptionResponse = new ExceptionResponse();
        this.exceptionResponse.setException(exception.name());
        this.exceptionResponse.setMessage(msg);
        this.exceptionResponse.setErrorCode(String.valueOf(code));
        this.exceptionResponse.setHttpStatus(httpStatus);
        this.exceptionResponse.setDateTime(LocalDateTime.now());
    }

}
