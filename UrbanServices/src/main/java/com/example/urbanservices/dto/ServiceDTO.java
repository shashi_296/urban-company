package com.example.urbanservices.dto;


import com.example.urbanservices.entity.Package;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class ServiceDTO {
    private long serviceId;
    private String name;
    List<Package> packageList;

}
