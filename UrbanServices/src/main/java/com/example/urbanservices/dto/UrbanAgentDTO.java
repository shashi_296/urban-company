package com.example.urbanservices.dto;

import com.example.urbanservices.entity.Address;
import com.example.urbanservices.entity.UrbanAgent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UrbanAgentDTO {
    private UrbanAgent urbanAgent;
    private Address address;

}
