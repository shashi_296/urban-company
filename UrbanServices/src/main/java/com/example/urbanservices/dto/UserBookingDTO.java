package com.example.urbanservices.dto;

import com.example.urbanservices.entity.Address;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Builder
public class UserBookingDTO {
    private long userId;
    private String name;
    private String mob;
    private String email;
    private Address address;
    private long bookingId;
    private long serviceId;
    private String serviceName;
    private long packageId;
    private String packageName;
    private double price;
    private long agentId;
    private String agentName;
    private Date bookingDate;

}
