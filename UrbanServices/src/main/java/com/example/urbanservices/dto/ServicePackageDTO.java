package com.example.urbanservices.dto;

import com.example.urbanservices.enums.PackageAvailabilityStatus;
import com.example.urbanservices.enums.UrbanServiceAvailabilityStatus;
import com.example.urbanservices.enums.UrbanServiceCategory;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class ServicePackageDTO {
    private long serviceId;
    private String serviceName;
    private long packageId;
    private String packageName;
    private double price;
    private UrbanServiceCategory category;
    private UrbanServiceAvailabilityStatus serviceAvailabilityStatus;
    private PackageAvailabilityStatus packageAvailabilityStatus;

}
