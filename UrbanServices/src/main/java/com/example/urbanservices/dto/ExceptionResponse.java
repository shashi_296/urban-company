package com.example.urbanservices.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionResponse {

    private String exception;
    private String message;
    private String errorCode;
    private HttpStatus httpStatus;
    private LocalDateTime dateTime;

}
