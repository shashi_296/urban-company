package com.example.urbanservices.dto;

import com.example.urbanservices.entity.Address;
import com.example.urbanservices.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private User user;
    private Address address;
}
