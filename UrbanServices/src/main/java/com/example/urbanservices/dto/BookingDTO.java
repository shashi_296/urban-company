package com.example.urbanservices.dto;
import com.example.urbanservices.enums.BookingStatus;
import com.example.urbanservices.enums.UrbanServiceCategory;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class BookingDTO  {
    private long bookingId;
    private long userId;
    private long packageId;
    private double price;
    private long serviceId;
    private long agentId;
    private UrbanServiceCategory category;
    private BookingStatus status;
    private UrbanAgentDTO urbanAgentDTO;
}
