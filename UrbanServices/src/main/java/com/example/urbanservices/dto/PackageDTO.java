package com.example.urbanservices.dto;


import com.example.urbanservices.enums.PackageAvailabilityStatus;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class PackageDTO {
    private long packageId;
    private String packageName;
    private PackageAvailabilityStatus status;
    private double price;
}
