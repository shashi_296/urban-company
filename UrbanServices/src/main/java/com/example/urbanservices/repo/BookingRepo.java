package com.example.urbanservices.repo;

import com.example.urbanservices.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookingRepo extends JpaRepository<Booking,Long> {
     Optional<Booking> findByBookingId(long bookingId);
     List<Booking> findAllByUserId(long userId);
}
