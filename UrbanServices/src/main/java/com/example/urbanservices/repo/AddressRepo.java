package com.example.urbanservices.repo;

import com.example.urbanservices.entity.Address;
import com.example.urbanservices.enums.EntityTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressRepo extends JpaRepository<Address,Long> {
    @Query("SELECT a FROM Address a WHERE a.entityId = :id and a.entityTypes = :entityTypes")
    Optional<Address> findByEntityIdEntityType(long id, EntityTypes entityTypes);
}
