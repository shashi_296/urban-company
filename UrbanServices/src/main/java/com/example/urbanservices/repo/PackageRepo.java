package com.example.urbanservices.repo;

import com.example.urbanservices.entity.Package;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PackageRepo extends JpaRepository<Package,Long> {
     Optional<Package> findByPackageId(long packageId);
}
