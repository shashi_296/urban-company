package com.example.urbanservices.repo;

import com.example.urbanservices.entity.UrbanAgent;
import com.example.urbanservices.enums.UrbanAgentAvailability;
import com.example.urbanservices.enums.UrbanServiceCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UrbanAgentRepo extends JpaRepository<UrbanAgent,Long> {
    Optional<UrbanAgent> findByEmail(String email);
    @Query("SELECT u FROM UrbanAgent u where u.status = 'AVAILABLE' and u.specialist = :category ")
    List<UrbanAgent> findBySpecialist(UrbanServiceCategory category);

    @Query("UPDATE UrbanAgent u SET u.status = :status where u.id = :id ")
    void updateStatus(long id, UrbanAgentAvailability status);
}
