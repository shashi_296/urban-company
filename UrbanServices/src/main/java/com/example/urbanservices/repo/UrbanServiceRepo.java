package com.example.urbanservices.repo;

import com.example.urbanservices.dto.ServicePackageDTO;
import com.example.urbanservices.entity.UrbanService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UrbanServiceRepo extends JpaRepository<UrbanService,Long> {
    Optional<UrbanService> findByServiceId(long serviceId);

    @Query("SELECT new com.example.urbanservices.dto.ServicePackageDTO(u.serviceId,u.name,p.packageId,p.packageName,p.price,u.category,u.status,p.status) "
            + "FROM Package p INNER JOIN p.service u")
    List<ServicePackageDTO> fetchServicePackageDTO();


}
