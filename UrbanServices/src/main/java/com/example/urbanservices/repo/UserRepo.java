package com.example.urbanservices.repo;

import com.example.urbanservices.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User,Long> {
    Optional<User> findByEmail(String emailId);
    Optional<User> findByMob(String mob);
}
