package com.example.urbanservices.entity;
import com.example.urbanservices.enums.PackageAvailabilityStatus;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Getter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "package")
public class Package implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long packageId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "serviceId",insertable = false,updatable = false)
    @Fetch(FetchMode.JOIN)
    private UrbanService service;

    private String packageName;

    @Enumerated(EnumType.STRING)
    private PackageAvailabilityStatus status;
    private double price;

    @CreationTimestamp
    private Date createdAt;
    @UpdateTimestamp
    private Date updatedAt;


}
