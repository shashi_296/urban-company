package com.example.urbanservices.entity;

import com.example.urbanservices.enums.BookingStatus;
import com.example.urbanservices.enums.UrbanServiceCategory;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "booking")
public class Booking {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private long bookingId;
    private long userId;
    private long packageId;
    private long serviceId;
    private long agentId;
    private double price;

    @Enumerated(EnumType.STRING)
    private UrbanServiceCategory category;

    @Enumerated(EnumType.STRING)
    private BookingStatus status;
    @CreationTimestamp
    private Date createdAt;
    @UpdateTimestamp
    private Date updatedAt;

}
