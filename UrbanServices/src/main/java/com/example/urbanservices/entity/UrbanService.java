package com.example.urbanservices.entity;

import com.example.urbanservices.enums.UrbanServiceAvailabilityStatus;
import com.example.urbanservices.enums.UrbanServiceCategory;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "service")
public class UrbanService {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long serviceId;
    private String name;

    @OneToMany(targetEntity = Package.class ,mappedBy = "service", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Package> packageList;

    @Enumerated(EnumType.STRING)
    private UrbanServiceAvailabilityStatus status;
    @Enumerated(EnumType.STRING)
    private UrbanServiceCategory category;

    @CreationTimestamp
    private Date createdAt;
    @UpdateTimestamp
    private Date updatedAt;

}
