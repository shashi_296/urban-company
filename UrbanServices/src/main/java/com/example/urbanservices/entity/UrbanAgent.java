package com.example.urbanservices.entity;

import com.example.urbanservices.enums.Gender;
import com.example.urbanservices.enums.UrbanAgentAvailability;
import com.example.urbanservices.enums.UrbanServiceCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "urban_agent")
public class UrbanAgent {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    private String name;
    private int age;
    private String mob;
    private String email;

    @Enumerated(EnumType.STRING)
    private UrbanAgentAvailability status;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Enumerated(EnumType.STRING)
    private UrbanServiceCategory specialist;
    @CreationTimestamp
    private Date onboarded;

}
