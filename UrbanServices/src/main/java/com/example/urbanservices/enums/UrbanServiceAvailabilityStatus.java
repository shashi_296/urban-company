package com.example.urbanservices.enums;

public enum UrbanServiceAvailabilityStatus {
    AVAILABLE,
    NOT_AVAILABLE,
    TEMPORARY_NOT_AVAILABLE,
    NOT_SERVED_THIS_AREA
}
