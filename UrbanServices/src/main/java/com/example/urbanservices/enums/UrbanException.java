package com.example.urbanservices.enums;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum UrbanException {

    BOOKING_FAILED(
            "BOOKING_FAILED",
            500,
            "Failed to Book",
            HttpStatus.INTERNAL_SERVER_ERROR
    ),
    BOOKING_NOT_FOUND(
            "BOOKING_NOT_FOUND",
            404,
            "Booking not found.",
            HttpStatus.NOT_FOUND
    ),
    BOOKING_SERVICE_DOWN(
            "BOOKING_SERVICE_DOWN",
            4001,
            "Service Down",
            HttpStatus.SERVICE_UNAVAILABLE
    ),
    CANCEL_FAILED(
            "CANCEL_FAILED_EXCEPTION",
            404,
            "Resource not found.",
            HttpStatus.NOT_FOUND
    ),
    DB_CONNECTION_FAILURE(
            "DB_CONNECTION_FAILURE_EXCEPTION",
            4001,
            "Connection Failed",
            HttpStatus.REQUEST_TIMEOUT
    ),
    PACKAGE_NOT_FOUND(
            "PACKAGE_NOT_FOUND",
            4002,
            "Package not found.",
            HttpStatus.NOT_FOUND
    ),
    SERVICE_NOT_FOUND(
            "SERVICE_NOT_FOUND",
            4003,
            "Service not found.",
            HttpStatus.NOT_FOUND
    ),
    USER_NOT_FOUND(
            "USER_NOT_FOUND",
            4003,
            "User not found.",
            HttpStatus.NOT_FOUND
    ),
    UPDATE_FAILED(
            "UPDATE_FAILED",
            4004,
            "Failed to update booking.",
            HttpStatus.INTERNAL_SERVER_ERROR
    ),
    SOMETHING_WENT_WRONG(
            "SOMETHING_WENT_WRONG",
            4005,
            "Something went wrong...we are working on it.",
            HttpStatus.SERVICE_UNAVAILABLE
    ),
    AGENT_NOT_FOUND(
            "AGENT_NOT_FOUND",
            4001,
            "Resource not found.",
            HttpStatus.NOT_FOUND
    ),
    ONBOARD_FAILED(
            "ONBOARD_FAILED",
            4001,
            "Resource not found.",
            HttpStatus.NOT_ACCEPTABLE
    ),
    NOT_FOUND(
            "NOT_FOUND",
            4001,
            "Resource not found.",
            HttpStatus.NOT_FOUND
    ),
    INVALID_REQUEST(
            "INVALID_REQUEST",
            5001,
            "Invalid request.",
            HttpStatus.BAD_REQUEST
    ),
    BAD_REQUEST(
            "BAD_REQUEST",
            5002,
            "Invalid data sent.",
            HttpStatus.BAD_REQUEST
    ),
    BAD_REQUEST_SSO_TOKEN_MISSING(
            "BAD_REQUEST_SSO_TOKEN_MISSING",
            5004,
            "Invalid data sent, sso token missing",
            HttpStatus.BAD_REQUEST
    ),
    UNABLE_TO_PROCESS(
            "UNABLE_TO_PROCESS",
            6001,
            "Unable to process current request.",
            HttpStatus.UNPROCESSABLE_ENTITY
    ),
    UNABLE_TO_ADD_PARAMS(
            "UNABLE_TO_ADD_PARAMS",
            6007,
            "Unable to add params.",
            HttpStatus.UNPROCESSABLE_ENTITY
    ),
    UNABLE_TO_CALL_HTTP(
            "UNABLE_TO_CALL_HTTP",
            6008,
            "Not able to call http.",
            HttpStatus.UNPROCESSABLE_ENTITY
    ),
    UNAUTHORIZED_ACCESS(
            "UNAUTHORIZED_ACCESS",
            70001,
            "Not authorized to perform this action.",
            HttpStatus.UNAUTHORIZED
    ),
    UNAUTHORIZED_USER(
            "NON_EXISTING_USER",
            70002,
            "User does not exist to perform this action.",
            HttpStatus.UNAUTHORIZED
    ),
    INTERNAL_SERVER_ERROR(
            "INTERNAL_SERVER_ERROR",
            80001,
            "Unknown error happened.",
            HttpStatus.INTERNAL_SERVER_ERROR
    ),
    SYSTEM_IN_MAINTENANCE_MODE(
            "SYSTEM_IN_MAINTENANCE_MODE",
            90001,
            "System is under maintenance mode.",
            HttpStatus.SERVICE_UNAVAILABLE
    );

    private Integer code;
    private String status;
    private String message;
    private HttpStatus httpStatus;

    UrbanException(final String status, final Integer code, final String message,
                   final HttpStatus httpStatus) {
        this.status = status;
        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }
}
