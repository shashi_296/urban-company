package com.example.urbanservices.enums;

public enum Gender {
    MALE,
    FEMALE
}
