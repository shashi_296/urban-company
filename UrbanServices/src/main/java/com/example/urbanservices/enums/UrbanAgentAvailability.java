package com.example.urbanservices.enums;

public enum UrbanAgentAvailability {
    AVAILABLE,
    RESERVED,
    TEMPORARY_NOT_AVAILABLE
}
