package com.example.urbanservices.enums;

public enum BookingStatus {
    CREATED,
    UPDATED,
    SUCCESS,
    FAILED,
    CANCELLED,
    RETRY
}
