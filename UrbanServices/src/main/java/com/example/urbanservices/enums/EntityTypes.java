package com.example.urbanservices.enums;

public enum EntityTypes {
    USER,
    AGENT,
    SALON
}
