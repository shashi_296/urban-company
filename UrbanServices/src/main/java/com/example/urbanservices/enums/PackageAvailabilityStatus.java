package com.example.urbanservices.enums;

public enum PackageAvailabilityStatus {
    AVAILABLE,
    NOT_AVAILABLE
}
