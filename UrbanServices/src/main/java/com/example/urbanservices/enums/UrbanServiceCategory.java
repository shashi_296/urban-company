package com.example.urbanservices.enums;

public enum UrbanServiceCategory {
    SALON,
    MASSAGE,
    CLEANING,
    REPAIRING
}
