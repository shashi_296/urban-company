package com.example.urbanservices.controller;

import com.example.urbanservices.dto.UrbanAgentDTO;
import com.example.urbanservices.dto.UserDTO;
import com.example.urbanservices.service.impl.OnBoardServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import static com.example.urbanservices.constants.Constant.*;

@Slf4j
@RestController
@RequestMapping(ONBOARD_API_URL)
public class UrbanOnboardController {

    private final OnBoardServiceImpl onBoardService;
    public UrbanOnboardController(OnBoardServiceImpl onBoardService) {
        this.onBoardService = onBoardService;
    }


    @PostMapping(value = USER)
    public String registerUser(@RequestBody UserDTO userDTO) {
        log.info("Input param userDTO:{}", userDTO);
        return onBoardService.onBoardUser(userDTO);
    }

    @PostMapping(value = AGENT)
    public String registerAgent(@RequestBody UrbanAgentDTO urbanAgentDTO) {
        log.info("Input param UrbanAgentDTO:{}", urbanAgentDTO);
        return onBoardService.onBoardAgent(urbanAgentDTO);
    }

    @GetMapping(value = AGENT_DETAIL)
    public UrbanAgentDTO fetchAgent(@RequestParam long id) {
        log.info("Input param agentId:{}", id);
        return onBoardService.getAgent(id);
    }

    @GetMapping(value = USER_DETAIL)
    public UserDTO fetchUser(@RequestParam long id) {
        log.info("Input param userId:{}", id);
        return onBoardService.getUser(id);
    }
}
