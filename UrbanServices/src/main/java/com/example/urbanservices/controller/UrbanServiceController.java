package com.example.urbanservices.controller;

import com.example.urbanservices.constants.Constant;
import com.example.urbanservices.dto.BookingDTO;
import com.example.urbanservices.dto.PackageDTO;
import com.example.urbanservices.dto.ServicePackageDTO;
import com.example.urbanservices.service.impl.BookingServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping(Constant.SERVICE_API_URL)
public class UrbanServiceController {

    private final BookingServiceImpl bookingService;

    public UrbanServiceController(BookingServiceImpl bookingService) {
        this.bookingService = bookingService;
    }


    @GetMapping(value = Constant.ALL_SERVICES)
    public List<ServicePackageDTO> fetchAllServices(@RequestParam long userId) {
        log.info("Input param userId:{}", userId);
        return bookingService.getAllServices(userId);
    }

    @GetMapping(value = Constant.ALL_PACKAGES)
    public Set<PackageDTO> fetchAllPackages(@RequestParam long userId, @RequestParam long serviceId) {
        log.info("Input param userId:{} ,serviceId:{}", userId, serviceId);
        return bookingService.getAllPackages(userId,serviceId);
    }

    @GetMapping(value = Constant.PACKAGE_DETAIL)
    public PackageDTO fetchPackageDetails(@RequestParam long userId,@RequestParam long packageId) {
        log.info("Input param userId:{},packageId: {}", userId, packageId);
        return bookingService.getPackageDetails(userId, packageId);
    }

    @PostMapping(value = Constant.CREATE_BOOKING)
    public BookingDTO createBooking(@RequestBody BookingDTO bookingDTO) {
        log.info("Input param BookingDTO:{}", bookingDTO);
        return bookingService.createBooking(bookingDTO);
    }

    @PostMapping(value = Constant.UPDATE_BOOKING)
    public BookingDTO updateBooking(@RequestBody BookingDTO bookingDTO) {
        log.info("Input param BookingDTO:{}", bookingDTO);
        return bookingService.updateBooking(bookingDTO);
    }

    @PostMapping(value = Constant.CANCEL_BOOKING)
    public BookingDTO cancelBooking(@RequestBody BookingDTO bookingDTO) {
        log.info("Input param BookingDTO:{}", bookingDTO);
        return bookingService.cancelBooking(bookingDTO);
    }

    @GetMapping(value = Constant.HISTORY)
    public List<BookingDTO> fetchPastBooking(@RequestParam long userId) {
        log.info("Input param userId:{}", userId);
        return bookingService.getPastBooking(userId);
    }
}
