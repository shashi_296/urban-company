package com.example.urbanservices.constants;

public class ErrorConstant {
    private ErrorConstant(){}
    public static final String BOOKING_SERVICE_DOWN = "Booking service down";
    public static final String DB_SERVICE_DOWN = "DB service down";
    public static final String SOMETHING_WENT_WRONG = "Something went wrong...we are working on it";
    public static final String DB_CONNECTION_FAILURE = "Currently DB service is not available";
    public static final String INVALID_USER = "Invalid user id";
    public static final String NO_USER_FOUND = "No user found";





}
