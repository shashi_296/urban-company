package com.example.urbanservices.constants;

public class Constant {
    private Constant() {}

    public static final String SERVICE_API_URL = "";
    public static final String CREATE_BOOKING = "create";
    public static final String UPDATE_BOOKING = "update";
    public static final String CANCEL_BOOKING = "cancel";
    public static final String ALL_SERVICES = "services";
    public static final String ALL_PACKAGES = "packages";
    public static final String PACKAGE_DETAIL = "package_detail";
    public static final String HISTORY = "history";

    public static final String ONBOARD_API_URL = "register";
    public static final String USER = "user";
    public static final String AGENT = "agent";

    public static final String AGENT_DETAIL = "detail/agent";
    public static final String USER_DETAIL = "detail/user";




}
