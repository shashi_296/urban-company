package com.example.urbanservices.service.impl;

import com.example.urbanservices.dto.UrbanAgentDTO;
import com.example.urbanservices.dto.UserDTO;
import com.example.urbanservices.entity.Address;
import com.example.urbanservices.entity.UrbanAgent;
import com.example.urbanservices.entity.User;
import com.example.urbanservices.enums.EntityTypes;
import com.example.urbanservices.enums.UrbanException;
import com.example.urbanservices.exception.UrbanGlobalException;
import com.example.urbanservices.repo.AddressRepo;
import com.example.urbanservices.repo.UrbanAgentRepo;
import com.example.urbanservices.repo.UserRepo;
import com.example.urbanservices.service.OnBoardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.example.urbanservices.constants.ErrorConstant.DB_CONNECTION_FAILURE;
import static com.example.urbanservices.constants.ErrorConstant.SOMETHING_WENT_WRONG;

@Slf4j
@Service
public class OnBoardServiceImpl implements OnBoardService {

    private final UserRepo userRepo;
    private final AddressRepo addressRepo;
    private final UrbanAgentRepo urbanAgentRepo;

    public OnBoardServiceImpl(UserRepo userRepo, AddressRepo addressRepo, UrbanAgentRepo urbanAgentRepo) {
        this.userRepo = userRepo;
        this.addressRepo = addressRepo;
        this.urbanAgentRepo = urbanAgentRepo;
    }

    /**
     * OnBoard user
     *
     * @param userDTO details of user
     * @return String return success/failure message
     */
    @Override
    public String onBoardUser(UserDTO userDTO) {
        User user = userDTO.getUser();
        try {
            if (!user.getEmail().isEmpty()) {
                userRepo.save(user);
                Optional<User> optionalUser = userRepo.findByEmail(user.getEmail());
                if (optionalUser.isPresent()) {
                    user = optionalUser.get();
                } else {
                    log.error(SOMETHING_WENT_WRONG);
                    throw new UrbanGlobalException(UrbanException.SOMETHING_WENT_WRONG, "User OnBoard Failed");
                }
            } else {
                log.error("User OnBoard Failed,Invalid email id");
                throw new UrbanGlobalException(UrbanException.BAD_REQUEST, "Invalid email id");
            }
        } catch (UrbanGlobalException e) {
            if (!e.getExceptionResponse().getMessage().isEmpty()) {
                throw e;
            }
            throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, DB_CONNECTION_FAILURE);
        } catch (Exception e) {
            log.error("User OnBoard Failed -" + e.getMessage());
        }
        Address address = userDTO.getAddress();
        if (addAddress(address, EntityTypes.USER, user.getUserId())) {
            return "User OnBoarded Successfully";
        } else {
            return "Failed to onBoard user,Invalid address";
        }
    }

    /**
     * OnBoard Agent
     *
     * @param urbanAgentDTO details of agent
     * @return String return success/failure message
     */
    @Override
    public String onBoardAgent(UrbanAgentDTO urbanAgentDTO) {
        UrbanAgent urbanAgent = urbanAgentDTO.getUrbanAgent();
        try {
            if (!urbanAgent.getEmail().isEmpty()) {
                urbanAgentRepo.save(urbanAgent);
                Optional<UrbanAgent> optionalUrbanAgent = urbanAgentRepo.findByEmail(urbanAgent.getEmail());
                if (optionalUrbanAgent.isPresent()) {
                    urbanAgent = optionalUrbanAgent.get();
                } else {
                    log.error(SOMETHING_WENT_WRONG);
                    throw new UrbanGlobalException(UrbanException.SOMETHING_WENT_WRONG, "Agent OnBoard Failed");
                }
            } else {
                log.error("Agent OnBoard Failed,Invalid email id");
                throw new UrbanGlobalException(UrbanException.BAD_REQUEST, "Invalid email id");
            }
        } catch (UrbanGlobalException e) {
            if (!e.getExceptionResponse().getMessage().isEmpty()) {
                throw e;
            }
            throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE,DB_CONNECTION_FAILURE);
        } catch (Exception e) {
            log.error("Agent OnBoard Failed -" + e.getMessage());
        }
        Address address = urbanAgentDTO.getAddress();
        if (addAddress(address, EntityTypes.AGENT, urbanAgent.getId())) {
            return "Agent OnBoarded Successfully";
        } else {
            return "Failed to onBoard agent,Invalid address";
        }
    }

    /**
     * Add address
     *
     * @param address details of address
     * @param entityTypes can be user/agent/salon
     * @param id entity id
     * @return true/false addition of address
     */
    private boolean addAddress(Address address, EntityTypes entityTypes, long id) {
        if (null != address) {
            try {
                if (id > 0) {
                    address.setEntityTypes(entityTypes);
                    address.setEntityId(id);
                    addressRepo.save(address);
                    return true;
                } else {
                    log.error("Invalid id,Failed to onBoard");
                    throw new UrbanGlobalException(UrbanException.BAD_REQUEST, "Invalid id");
                }
            } catch (UrbanGlobalException e) {
                if (!e.getExceptionResponse().getMessage().isEmpty()) {
                    throw e;
                }
                throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, DB_CONNECTION_FAILURE);
            } catch (Exception e) {
                log.error("Address addition Failed -" + e.getMessage());
            }
        } else {
            log.error("Invalid address");
        }
        return false;
    }

    /**
     * Fetches agent
     *
     * @param id id of agent
     * @return UrbanAgentDTO details of agent
     */
    @Override
    public UrbanAgentDTO getAgent(long id) {
        try {
            Optional<UrbanAgent> optionalUrbanAgent = urbanAgentRepo.findById(id);
            if (optionalUrbanAgent.isPresent()) {
                UrbanAgentDTO urbanAgentDTO = new UrbanAgentDTO();
                urbanAgentDTO.setUrbanAgent(optionalUrbanAgent.get());
                Optional<Address> optionalAddress = addressRepo.findByEntityIdEntityType(id, EntityTypes.AGENT);
                if (optionalAddress.isPresent()) {
                    urbanAgentDTO.setAddress(optionalAddress.get());
                    return urbanAgentDTO;
                } else {
                    log.error("No address mapped with agentId:{}", id);
                    throw new UrbanGlobalException(UrbanException.NOT_FOUND, "Address not found");
                }
            } else {
                log.error("Invalid agent id");
                throw new UrbanGlobalException(UrbanException.AGENT_NOT_FOUND, "Agent not found");
            }
        } catch (UrbanGlobalException e) {
            if (!e.getExceptionResponse().getMessage().isEmpty()) {
                throw e;
            }
            throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, DB_CONNECTION_FAILURE);
        } catch (Exception e) {
            log.error("Failed to fetch agent details -" + e.getMessage());
            throw new UrbanGlobalException(UrbanException.AGENT_NOT_FOUND, "Agent not found");

        }
    }

    /**
     * Fetches user
     *
     * @param id id of user
     * @return user details
     */
    @Override
    public UserDTO getUser(long id) {
        try {
            Optional<User> optionalUser = userRepo.findById(id);
            if (optionalUser.isPresent()) {
                UserDTO userDTO = new UserDTO();
                userDTO.setUser(optionalUser.get());
                Optional<Address> optionalAddress = addressRepo.findByEntityIdEntityType(id, EntityTypes.USER);
                if (optionalAddress.isPresent()) {
                    userDTO.setAddress(optionalAddress.get());
                    return userDTO;
                } else {
                    log.error("No address mapped with userId:{}", id);
                    throw new UrbanGlobalException(UrbanException.NOT_FOUND, "Address not found");
                }
            } else {
                log.error("Invalid user id");
                throw new UrbanGlobalException(UrbanException.USER_NOT_FOUND, "User not found");
            }
        } catch (UrbanGlobalException e) {
            if (!e.getExceptionResponse().getMessage().isEmpty()) {
                throw e;
            }
            throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, DB_CONNECTION_FAILURE);
        } catch (Exception e) {
            log.error("Failed to fetch user details -" + e.getMessage());
            throw new UrbanGlobalException(UrbanException.USER_NOT_FOUND, "User not found");

        }
    }
}
