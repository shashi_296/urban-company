package com.example.urbanservices.service.impl;


import com.example.urbanservices.dto.BookingDTO;
import com.example.urbanservices.dto.PackageDTO;
import com.example.urbanservices.dto.ServicePackageDTO;
import com.example.urbanservices.dto.UrbanAgentDTO;
import com.example.urbanservices.entity.*;
import com.example.urbanservices.entity.Package;
import com.example.urbanservices.enums.*;
import com.example.urbanservices.exception.UrbanGlobalException;
import com.example.urbanservices.mapper.BookingMapper;
import com.example.urbanservices.mapper.PackageMapper;
import com.example.urbanservices.repo.*;
import com.example.urbanservices.service.BookingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import static com.example.urbanservices.constants.ErrorConstant.*;


@Slf4j
@Service
public class BookingServiceImpl implements BookingService {

    private final BookingRepo bookingRepo;
    private final PackageRepo packageRepo;
    private final UserRepo userRepo;
    private final UrbanServiceRepo urbanServiceRepo;
    private final UrbanAgentRepo urbanAgentRepo;
    private final AddressRepo addressRepo;
    private final Random rand;

    public BookingServiceImpl(BookingRepo bookingRepo, PackageRepo packageRepo,UserRepo userRepo, UrbanServiceRepo urbanServiceRepo,UrbanAgentRepo urbanAgentRepo,AddressRepo addressRepo) throws NoSuchAlgorithmException {
        this.bookingRepo = bookingRepo;
        this.packageRepo = packageRepo;
        this.userRepo = userRepo;
        this.urbanServiceRepo = urbanServiceRepo;
        this.urbanAgentRepo = urbanAgentRepo;
        this.addressRepo = addressRepo;
        this.rand = SecureRandom.getInstanceStrong();
    }

    /**
     * Check for user
     *
     * @param id  of user
     * @return boolean availability of user
     */
    private boolean checkUser(long id){
        try {
            Optional<User> optionalUser = userRepo.findById(id);
            if(optionalUser.isPresent()){
                return true;
            }else{
                log.error("Invalid user,No user is available with userId:{}",id);
                return false;
            }
        } catch (Exception e) {
            throw new UrbanGlobalException(UrbanException.USER_NOT_FOUND,"Invalid user id");
        }
    }

    /**
     * Fetches List of all urban services provided
     *
     * @param userId id of user
     * @return List<UrbanService> List of all urban services
     */
    public List<ServicePackageDTO> getAllServices(long userId) {
        log.info("Fetching all services for user:{}", userId);
        try {
            if (checkUser(userId)) {
                Optional<List<ServicePackageDTO>> optionalServicePackageDTOS = Optional.of(urbanServiceRepo.fetchServicePackageDTO());
                if (optionalServicePackageDTOS.get().isEmpty()) {
                    throw new UrbanGlobalException(UrbanException.SERVICE_NOT_FOUND, "No Service available");
                } else {
                    log.info("All urban services fetched successfully for user: {} ", userId);
                    return optionalServicePackageDTOS.get();
                }
            } else {
                log.error(NO_USER_FOUND);
                throw new UrbanGlobalException(UrbanException.USER_NOT_FOUND, INVALID_USER);
            }
        } catch (UrbanGlobalException e) {
            if (!e.getExceptionResponse().getMessage().isEmpty()) {
                throw e;
            }
            log.info(DB_CONNECTION_FAILURE);
            throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, DB_SERVICE_DOWN);
        } catch (Exception e) {
            log.error("Failed to fetch services-{}", e.getMessage());
            throw new UrbanGlobalException(UrbanException.SOMETHING_WENT_WRONG);
        }
    }

    /**
     * Fetches List of all urban packages inside a service
     *
     * @param userId    id of user
     * @param serviceId id of service
     * @return List<Package> List of all packages inside an urban service
     */
    public Set<PackageDTO> getAllPackages(long userId, long serviceId) {
        log.info("Fetching all packages for user: {} for service: {} :", userId, serviceId);
        try {
            if (checkUser(userId)) {
                Optional<UrbanService> optionalUrbanService = urbanServiceRepo.findByServiceId(serviceId);
                if (optionalUrbanService.isEmpty()) {
                    throw new UrbanGlobalException(UrbanException.SERVICE_NOT_FOUND, "Invalid service id");
                } else {
                    UrbanService urbanService = optionalUrbanService.get();
                    log.info("Urban services fetched successfully");
                    if (urbanService.getPackageList().isEmpty()) {
                        log.info("No Package is available for this service-{}", urbanService.getName());
                    } else {
                        log.info("All Packages fetched successfully for user: {} with service:{}", userId, serviceId);
                    }
                    return PackageMapper.INSTANCE.convertListToDto(urbanService.getPackageList());
                }
            }
            else {
                log.error(NO_USER_FOUND);
                throw new UrbanGlobalException(UrbanException.USER_NOT_FOUND, INVALID_USER);
            }
        } catch (UrbanGlobalException e) {
            if (!e.getExceptionResponse().getMessage().isEmpty()) {
                throw e;
            }
            log.info(DB_CONNECTION_FAILURE);
            throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, DB_SERVICE_DOWN);
        } catch (Exception e) {
            log.error("Failed to fetch package-{}", e.getMessage());
            throw new UrbanGlobalException(UrbanException.SOMETHING_WENT_WRONG);
        }
    }

    /**
     * Fetches a package details
     *
     * @param userId    id of user
     * @param packageId id of package
     * @return Optional<Package> Details of a package inside an urban services
     */
    public PackageDTO getPackageDetails(long userId, long packageId) {
        log.info("Fetching package details for user: {} with package: {}:", userId, packageId);
        if (checkPackageAvailability(packageId)) {
            try {
                if (checkUser(userId)) {
                    Optional<Package> optionalPackage = packageRepo.findByPackageId(packageId);
                    if (optionalPackage.isEmpty()) {
                        throw new UrbanGlobalException(UrbanException.PACKAGE_NOT_FOUND, "Invalid package");
                    } else {
                        Package pack = optionalPackage.get();
                        log.info("Package fetched successfully");
                        return PackageMapper.INSTANCE.convertToDto(pack);
                    }
                } else {
                    log.error(NO_USER_FOUND);
                    throw new UrbanGlobalException(UrbanException.USER_NOT_FOUND, INVALID_USER);
                }
            } catch (UrbanGlobalException e) {
                if (!e.getExceptionResponse().getMessage().isEmpty()) {
                    throw e;
                }
                log.info(DB_CONNECTION_FAILURE);
                throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, DB_SERVICE_DOWN);
            } catch (Exception e) {
                log.error("Failed to fetch package-{}", e.getMessage());
                throw new UrbanGlobalException(UrbanException.SOMETHING_WENT_WRONG);
            }
        } else {
            log.error("Failed to fetch package");
            throw new UrbanGlobalException(UrbanException.PACKAGE_NOT_FOUND, "Failed to fetch package,No Package is available");
        }
    }

    /**
     * Created a unique id
     *
     * @return id ,a random 6-digit number
     */
    private long randomIdGenerator() {
        return this.rand.nextLong(999999);
    }

    /**
     * Check package availability
     *
     * @return true/false ,depending on availability
     */
    private boolean checkPackageAvailability(long packageId) {
        try {
            Optional<Package> optionalPackage = packageRepo.findByPackageId(packageId);
            if (optionalPackage.isEmpty()) {
                log.info("No Package is available for packageId:{}", packageId);
                return false;
            } else {
                if (!optionalPackage.get().getStatus().equals(PackageAvailabilityStatus.AVAILABLE)) {
                    return false;
                }
            }
        } catch (Exception e) {
            log.error("Unable to connect to db");
        }
        return true;
    }

    /**
     * Check service availability
     *
     * @return true/false ,depending on availability
     */
    private boolean checkServiceAvailability(long serviceId) {
        try {
            Optional<UrbanService> optionalUrbanService = urbanServiceRepo.findByServiceId(serviceId);
            if (optionalUrbanService.isEmpty()) {
                log.info("No Urban Service is available for serviceId:{}", serviceId);
                return false;
            } else {
                if (!optionalUrbanService.get().getStatus().equals(UrbanServiceAvailabilityStatus.AVAILABLE)) {
                    return false;
                }
            }
        } catch (Exception e) {
            log.error("Unable to connect to db");
        }
        return true;
    }

    /**
     * Created a booking
     *
     * @param bookingDTO bookingId of user
     * @return Optional<Booking> Booking details of a package inside an urban service
     */
    @Override
    public BookingDTO createBooking(BookingDTO bookingDTO) {
        log.info("Booking initiated for user: {} for package:{},serviceId:{}", bookingDTO.getUserId(), bookingDTO.getPackageId(), bookingDTO.getServiceId());
        if (checkUser(bookingDTO.getUserId())) {
            if (checkServiceAvailability(bookingDTO.getServiceId())) {
                if (checkPackageAvailability(bookingDTO.getPackageId())) {
                    long bookingId = randomIdGenerator();
                    try {
                        Booking booking = Booking.builder().bookingId(bookingId).serviceId(bookingDTO.getServiceId()).userId(bookingDTO.getUserId()).packageId(bookingDTO.getPackageId()).status(BookingStatus.CREATED).build();
                        //Any additional steps can be called here e.g. payments
                        booking.setStatus(BookingStatus.SUCCESS);
                        List<UrbanAgent> optionalUrbanAgent = urbanAgentRepo.findBySpecialist(bookingDTO.getCategory());
                        if (optionalUrbanAgent.isEmpty()) {
                            log.error("No Agent is available to provide service");
                            throw new UrbanGlobalException(UrbanException.AGENT_NOT_FOUND, "No agent is available currently");
                        } else {
                            UrbanAgent urbanAgent = optionalUrbanAgent.get(0);
                            urbanAgent.setStatus(UrbanAgentAvailability.RESERVED);
                            urbanAgentRepo.save(urbanAgent);
                        }
                        Optional<Package> optionalPackage = packageRepo.findByPackageId(bookingDTO.getPackageId());
                        if (optionalPackage.isPresent()) {
                            booking.setPrice(optionalPackage.get().getPrice());
                            booking.setAgentId(optionalUrbanAgent.get(0).getId());
                            booking.setCategory(bookingDTO.getCategory());
                            bookingRepo.save(booking);
                        } else {
                            log.error("Currently package is not available for booking");
                            throw new UrbanGlobalException(UrbanException.PACKAGE_NOT_FOUND, "No package is available currently");
                        }
                        Optional<Booking> optionalBooking = bookingRepo.findByBookingId(bookingId);
                        if (optionalBooking.isEmpty()) {
                            throw new UrbanGlobalException(UrbanException.BOOKING_SERVICE_DOWN, BOOKING_SERVICE_DOWN);
                        } else {
                            log.info("Booking created successfully");
                            BookingDTO bookingDto = BookingMapper.INSTANCE.convertToDto(optionalBooking.get());
                            UrbanAgentDTO urbanAgentDTO = new UrbanAgentDTO();
                            Optional<Address> optionalAddress = addressRepo.findByEntityIdEntityType(optionalUrbanAgent.get(0).getId(), EntityTypes.AGENT);
                            if (optionalAddress.isPresent()) {
                                urbanAgentDTO.setUrbanAgent(optionalUrbanAgent.get(0));
                                urbanAgentDTO.setAddress(optionalAddress.get());
                                bookingDto.setUrbanAgentDTO(urbanAgentDTO);
                                return bookingDto;
                            } else {
                                log.error("Failed to fetch address of agent");
                                throw new UrbanGlobalException(UrbanException.AGENT_NOT_FOUND, "No agent is available currently");
                            }
                        }
                    } catch (UrbanGlobalException e) {
                        if (!e.getExceptionResponse().getMessage().isEmpty()) {
                            throw e;
                        }
                        log.info(DB_CONNECTION_FAILURE);
                        throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, "Failed to connect to db");
                    } catch (Exception e) {
                        log.error("Failed to create booking-{}", e.getMessage());
                        throw new UrbanGlobalException(UrbanException.SOMETHING_WENT_WRONG);
                    }
                } else {
                    log.error("Failed to create booking");
                    throw new UrbanGlobalException(UrbanException.BOOKING_FAILED, "Failed to create booking,No Package is available");
                }
            } else {
                log.error("Failed to create booking");
                throw new UrbanGlobalException(UrbanException.BOOKING_FAILED, "Failed to create booking,No Service is available");
            }
        } else {
            log.error(NO_USER_FOUND);
            throw new UrbanGlobalException(UrbanException.USER_NOT_FOUND, INVALID_USER);
        }
    }

    /**
     * Update a booking
     *
     * @param bookingDTO booking details
     * @return bookingDTO Updates booking details of a package inside an urban service
     */
    @Override
    public BookingDTO updateBooking(BookingDTO bookingDTO) throws UrbanGlobalException {
        log.info("Booking update initiated for user: {} for package:{},serviceId:{}", bookingDTO.getUserId(), bookingDTO.getPackageId(), bookingDTO.getServiceId());
        if (checkUser(bookingDTO.getUserId())) {
            if (checkServiceAvailability(bookingDTO.getServiceId())) {
                if (checkPackageAvailability(bookingDTO.getPackageId())) {
                    try {
                        Optional<Booking> optionalBooking = bookingRepo.findByBookingId(bookingDTO.getBookingId());
                        if (optionalBooking.isEmpty()) {
                            throw new UrbanGlobalException(UrbanException.BOOKING_NOT_FOUND, "No Booking details available");
                        } else {
                            Booking booking = optionalBooking.get();
                            booking.setServiceId(bookingDTO.getServiceId());
                            booking.setPackageId(bookingDTO.getPackageId());
                            booking.setStatus(BookingStatus.UPDATED);
                            bookingRepo.saveAndFlush(booking);
                            log.info("Booking updated successfully");
                            return BookingMapper.INSTANCE.convertToDto(booking);
                        }
                    } catch (UrbanGlobalException e) {
                        if (!e.getExceptionResponse().getMessage().isEmpty()) {
                            throw e;
                        }
                        log.info(DB_CONNECTION_FAILURE);
                        throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, DB_SERVICE_DOWN);
                    } catch (Exception e) {
                        log.error("Failed to update booking-{}", e.getMessage());
                        throw new UrbanGlobalException(UrbanException.SOMETHING_WENT_WRONG);
                    }
                } else {
                    log.error("Failed to update booking");
                    throw new UrbanGlobalException(UrbanException.UPDATE_FAILED, "Failed to update booking,No Package is available");
                }
            } else {
                log.error("Failed to update booking");
                throw new UrbanGlobalException(UrbanException.UPDATE_FAILED, "Failed to update booking,No Service is available");
            }
        } else {
            log.error(NO_USER_FOUND);
            throw new UrbanGlobalException(UrbanException.USER_NOT_FOUND, INVALID_USER);
        }
    }

    /**
     * Cancel a booking
     *
     * @param bookingDTO id of user
     * @return bookingDTO Cancel booking details of a package inside an urban service
     */
    @Override
    public BookingDTO cancelBooking(BookingDTO bookingDTO) {
        log.info("Booking cancel initiated for user: {} for package:{},serviceId:{},BookingId:{}", bookingDTO.getUserId(), bookingDTO.getPackageId(), bookingDTO.getServiceId(), bookingDTO.getBookingId());
        if (checkUser(bookingDTO.getUserId())) {
            if (checkServiceAvailability(bookingDTO.getServiceId())) {
                if (checkPackageAvailability(bookingDTO.getPackageId())) {
                    try {
                        Optional<Booking> optionalBooking = bookingRepo.findByBookingId(bookingDTO.getBookingId());
                        if (optionalBooking.isEmpty()) {
                            throw new UrbanGlobalException(UrbanException.BOOKING_NOT_FOUND, "No Booking details available");
                        } else {
                            Booking booking = optionalBooking.get();
                            booking.setServiceId(bookingDTO.getServiceId());
                            booking.setPackageId(bookingDTO.getPackageId());
                            booking.setStatus(BookingStatus.CANCELLED);
                            bookingRepo.saveAndFlush(booking);
                            log.info("Booking cancelled successfully");
                            return BookingMapper.INSTANCE.convertToDto(booking);
                        }
                    } catch (UrbanGlobalException e) {
                        if (!e.getExceptionResponse().getMessage().isEmpty()) {
                            throw e;
                        }
                        log.info("Currently DB service is not available");
                        throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, "DB service down");
                    } catch (Exception e) {
                        log.error("Failed to cancel booking-{}", e.getMessage());
                        throw new UrbanGlobalException(UrbanException.SOMETHING_WENT_WRONG);
                    }
                } else {
                    log.error("Failed to cancel booking");
                    throw new UrbanGlobalException(UrbanException.CANCEL_FAILED, "Failed to cancel booking,No Package is available");
                }
            } else {
                log.error("Failed to cancel booking");
                throw new UrbanGlobalException(UrbanException.CANCEL_FAILED, "Failed to cancel booking,No Service is available");
            }
        } else {
            log.error(NO_USER_FOUND);
            throw new UrbanGlobalException(UrbanException.USER_NOT_FOUND, INVALID_USER);
        }
    }

    /**
     * Fetches past booking history
     *
     * @param userId id of user
     * @return List<Booking> Gives all booking details of a user for booked urban services
     */
    public List<BookingDTO> getPastBooking(long userId) {
        log.info("Fetching past booking history for user: {}", userId);
        try {
            if (checkUser(userId)) {
                Optional<List<Booking>> optionalBookingList = Optional.of(bookingRepo.findAllByUserId(userId));
                if (optionalBookingList.get().isEmpty() ) {
                    log.info("Currently no booking records is available for user:{}", userId);
                    throw new UrbanGlobalException(UrbanException.BOOKING_NOT_FOUND, "No Records");
                } else {
                    log.info("Booking history fetched successfully");
                    List<BookingDTO> bookingDTOList = BookingMapper.INSTANCE.convertListToDto(optionalBookingList.get());
                    for(int i=0;i<bookingDTOList.size();i++){
                        Optional<UrbanAgent> optionalUrbanAgent = urbanAgentRepo.findById(bookingDTOList.get(i).getAgentId());
                        if(optionalUrbanAgent.isPresent()){
                            Booking booking = optionalBookingList.get().get(i);
                            UrbanAgentDTO urbanAgentDTO = new UrbanAgentDTO();
                            urbanAgentDTO.setUrbanAgent(optionalUrbanAgent.get());
                            Optional<Address> optionalAddress = addressRepo.findByEntityIdEntityType(optionalUrbanAgent.get().getId(),EntityTypes.AGENT);
                            optionalAddress.ifPresent(urbanAgentDTO::setAddress);
                            BookingDTO bookingDTO = BookingMapper.INSTANCE.convertToDto(booking);
                            bookingDTO.setUrbanAgentDTO(urbanAgentDTO);
                            bookingDTOList.get(i).setUrbanAgentDTO(urbanAgentDTO);
                        }else{
                            log.error("No Agent is available for this package");
                            throw new UrbanGlobalException(UrbanException.AGENT_NOT_FOUND);
                        }
                    }
                    return bookingDTOList;
                }
            } else {
                log.error(NO_USER_FOUND);
                throw new UrbanGlobalException(UrbanException.USER_NOT_FOUND, INVALID_USER);
            }
        } catch (UrbanGlobalException e) {
            if (!e.getExceptionResponse().getMessage().isEmpty()) {
                throw e;
            }
            log.info("Currently DB service is not available");
            throw new UrbanGlobalException(UrbanException.DB_CONNECTION_FAILURE, DB_SERVICE_DOWN);
        } catch (Exception e) {
            log.error("Failed to fetch booking details");
            throw new UrbanGlobalException(UrbanException.SOMETHING_WENT_WRONG);
        }

    }
}
