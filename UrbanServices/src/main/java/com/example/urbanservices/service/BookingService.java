package com.example.urbanservices.service;

import com.example.urbanservices.dto.BookingDTO;

public interface BookingService {
    BookingDTO createBooking(BookingDTO bookingDTO);
    BookingDTO updateBooking(BookingDTO bookingDTO);
    BookingDTO cancelBooking(BookingDTO bookingDTO);
}
