package com.example.urbanservices.service;

import com.example.urbanservices.dto.UrbanAgentDTO;
import com.example.urbanservices.dto.UserDTO;

public interface OnBoardService {
    String onBoardUser(UserDTO userDTO);
    String onBoardAgent(UrbanAgentDTO urbanAgentDTO);
    UserDTO getUser(long id);
    UrbanAgentDTO getAgent(long id);

}
