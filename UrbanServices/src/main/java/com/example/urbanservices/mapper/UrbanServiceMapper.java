package com.example.urbanservices.mapper;

import com.example.urbanservices.dto.ServiceDTO;
import com.example.urbanservices.entity.UrbanService;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper
public interface UrbanServiceMapper {
     UrbanServiceMapper INSTANCE = Mappers.getMapper(UrbanServiceMapper.class);
     ServiceDTO convertToDto(UrbanService service);
     List<ServiceDTO> convertListToDto(List<UrbanService> service);
     UrbanService convertToEntity(ServiceDTO serviceDTO);
     List<UrbanService> convertListToEntity(List<ServiceDTO> serviceDTO);

}
