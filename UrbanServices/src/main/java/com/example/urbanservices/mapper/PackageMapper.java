package com.example.urbanservices.mapper;

import com.example.urbanservices.dto.PackageDTO;
import com.example.urbanservices.entity.Package;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.Set;

@Mapper
public interface PackageMapper {
    PackageMapper INSTANCE = Mappers.getMapper(PackageMapper.class);
    PackageDTO convertToDto(Package aPackage);
    Set<PackageDTO> convertListToDto(Set<Package> packageList);
    Package convertToEntity(PackageDTO packageDTO);
    Set<Package> convertListToEntity(Set<PackageDTO> packageDTOList);
}
