package com.example.urbanservices.mapper;

import com.example.urbanservices.dto.BookingDTO;
import com.example.urbanservices.entity.Booking;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BookingMapper {
    BookingMapper INSTANCE = Mappers.getMapper(BookingMapper.class);
    BookingDTO convertToDto(Booking booking);
    List<BookingDTO> convertListToDto(List<Booking> bookingList);
    Booking convertToEntity(BookingDTO bookingDTO);
    List<Booking> convertListToEntity(List<BookingDTO> bookingDTOList);
}
