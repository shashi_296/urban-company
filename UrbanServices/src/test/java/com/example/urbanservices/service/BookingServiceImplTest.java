package com.example.urbanservices.service;

import com.example.urbanservices.dto.BookingDTO;
import com.example.urbanservices.dto.ExceptionResponse;
import com.example.urbanservices.dto.PackageDTO;
import com.example.urbanservices.entity.User;
import com.example.urbanservices.enums.BookingStatus;
import com.example.urbanservices.enums.PackageAvailabilityStatus;
import com.example.urbanservices.exception.UrbanGlobalException;
import com.example.urbanservices.repo.*;
import com.example.urbanservices.service.impl.BookingServiceImpl;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
class BookingServiceImplTest  {

    private BookingServiceImpl bookingServiceImpl;

    @Mock
    private UrbanAgentRepo urbanAgentRepo;

    @Mock
    private  BookingRepo bookingRepo;
    @Mock
    private  PackageRepo packageRepo;
    @Mock
    private  UserRepo userRepo;
    @Mock
    private  UrbanServiceRepo urbanServiceRepo;
    @Mock
    private  AddressRepo addressRepo;

    private  Random rand;

    @BeforeEach
    void setup() throws NoSuchAlgorithmException {
        bookingServiceImpl = new BookingServiceImpl(bookingRepo, packageRepo, userRepo, urbanServiceRepo, urbanAgentRepo, addressRepo);
    }

    BookingServiceImplTest() throws NoSuchAlgorithmException {
    }


    @Test
    void testGetAllServicesEmptyService() {
        User user = new User();
        when(userRepo.findById(3L)).thenReturn(Optional.of(user));
        UrbanGlobalException exception = assertThrows(UrbanGlobalException.class, () -> bookingServiceImpl.getAllServices(3).size());
        assertEquals("No Service available", exception.getExceptionResponse().getMessage());
    }

    @Test
    void testGetAllServicesEmptyService() {
        User user = new User();
        when(userRepo.findById(3L)).thenReturn(Optional.of(user));
        UrbanGlobalException exception = assertThrows(UrbanGlobalException.class, () -> bookingServiceImpl.getAllServices(3).size());
        assertEquals("No Service available", exception.getExceptionResponse().getMessage());
    }

    @Test
    void testGetAllPackages() {
        //assertEquals(1,bookingService.getAllPackages(1,123).size());
    }

    @Test
    void testGetPackageDetail() {
        //PackageDTO packageDTO = PackageDTO.builder().packageId(123).packageName("test_pack").status(PackageAvailabilityStatus.AVAILABLE).price(299).build();
        //assertEquals(packageDTO,bookingService.getPackageDetails(1,123,123));
    }

    @Test
    void testCreateBooking() {
        //BookingDTO bookingDTO = BookingDTO.builder().bookingId(1).userId(1).packageId(123).serviceId(123).status(BookingStatus.CREATED).build();
        //assertEquals(bookingDTO,bookingService.createBooking(1,123,123));
    }

    @Test
    void testUpdateBooking() {
        //BookingDTO bookingDTO = BookingDTO.builder().bookingId(1).userId(1).packageId(123).serviceId(123).status(BookingStatus.UPDATED).build();
        //assertEquals(bookingDTO,bookingService.updateBooking(1,123,123));
    }

    @Test
    void testCancelBooking() {
        //BookingDTO bookingDTO = BookingDTO.builder().bookingId(1).userId(1).packageId(123).serviceId(123).status(BookingStatus.CANCELLED).build();
        //assertEquals(bookingDTO,bookingService.cancelBooking(1,123,123));
    }

//    @Test
//    void testGetPastBooking() {
//        assertEquals(1,bookingService.getPastBooking(1).size());
//    }
}
