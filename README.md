## Urban Company

This a Java based Spring Boot Application in which we are going to design an online platform which helps user/customer to get their hair cut solution at home.
All Rest APIs will be exposed to user*

---


## Requirements and Goal of system

There will be certain services available for user

1. You will be able to signup/login to system.
2. You can search for hair cut packages.
3. You can schedule/reschedule/cancel your hair cut booking.
4. You can explore other urban services and packages.
5. You will be able to view your past bookings.
6. You can do online payments for urban services.
7. Review and feedback services is also there.
8. Membership and subscription plan are also there.

---


## References

1. [Java installation](https://docs.oracle.com/javase/10/install/installation-jdk-and-jre-macos.htm#JSJIG-GUID-2FE451B0-9572-4E38-A1A5-568B77B146DE).
2. [Spring Boot Application setup](https://spring.io/projects/spring-boot)
3. [Mysql Integration](https://spring.io/guides/gs/accessing-data-mysql/)
4. [Junit 5](https://www.journaldev.com/20834/junit5-tutorial)
